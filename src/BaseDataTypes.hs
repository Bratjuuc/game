module BaseDataTypes where
import Data.Monoid


data World = World {getEntities :: [Entity]}

--TODO

type Point = (Sum Integer, Sum Integer)

data Entity = Entity {getProperties ::[Property], position :: Point, image :: Char} | Null
data Property = Health {getCurrentHealth :: Int, maxHealth :: Int}
                 | PlayerControlled
                 | Projectile {direction :: Direction}
                 | Door {isLocked :: Bool, isOpened :: Bool}
                 | Book {text :: [String], page :: Int}
                 | Has {has :: Inventory}
                 | Orientation { orientation :: Direction}
                 | Blocks | Weak {integrity :: Int} | Lootable | Deadly | Void 
                 | Moveable {getWeight :: Int} 
                 | Powerful {getPower :: Int}
                 | Teleport {destination :: Point}

type Inventory = [(Item, Int)]
data Item = Item {description :: String, effect :: Action}
data Direction = Up' | Right' | Down' | Left' deriving (Eq, Bounded)

toDeltaX :: Direction -> Point
toDeltaX x = case x of
              Up' -> (Sum 0, Sum 1)
              Down' -> (Sum 0, Sum (-1))
              Left' -> (Sum (-1), Sum 0)
              Right' -> (Sum 1, Sum 0)
              


[defHealth, defPC, defProjectile,
    defDoor, defBook, defHas,
    defOrientation, defBlocks,
    defWeak, defLoot, defDeadly,
    defVoid, defMoveable, defPower, defTeleport] = map show [Health {}, PlayerControlled {}, Projectile {},
                                     Door {}, Book {}, Has {}, Orientation {},
                                     Blocks {}, Weak {}, Lootable {}, Deadly {}, Void {}, Moveable {}, Powerful {}, Teleport {}]


instance Show Property where
   show x = case x of
       Health {}           -> "Health"
       PlayerControlled {} -> "Player"
       Projectile {}       -> "Projectile"
       Door {}             -> "Door"
       Book {}             -> "Book"
       Has {}              -> "Has"
       Orientation {}      -> "Orientation"
       Blocks {}           -> "Blocks"
       Weak    {}          -> "Weak"
       Lootable {}         -> "Lootable"
       Deadly {}           -> "Deadly"
       Void {}             -> "Void"
       Moveable {}         -> "Moveable"
       Powerful {}         -> "Powerful"
       Teleport {}         -> "Teleport"

type Action = Entity -> Entity -> (Entity, Entity)
type System = World -> IO World



