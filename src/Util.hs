module Util where
import BaseDataTypes
import Data.Maybe (listToMaybe)

getFields :: String -> Entity -> [Property] --DONE
getFields str = filter ((str == ) <$> show) . getProperties

getField :: String -> Entity -> Maybe Property --DONE
getField str = listToMaybe . getFields str

fieldFilter :: String -> [Entity] -> [Entity]
fieldFilter fieldName = filter (not . null . getFields fieldName)

entMap :: (Entity -> Bool) -> (Entity -> Entity) ->  [Entity] -> [Entity]
entMap predicate f = map (\x -> if predicate x then f x else x)

entConcatMap :: (Entity -> Bool) -> (Entity -> [Entity]) ->  [Entity] -> [Entity]
entConcatMap predicate f = concatMap (\x -> if predicate x then f x else [x])