module QuadTree where

type Point = (Int, Int)
data Quadro a = Quadro a a a a deriving Show
data Quadrant a = Quadrant {lu :: Point, rb :: Point, val :: QuadTree a} deriving Show
data QuadTree a = Empty | Bucket a Point | QuadTree (Quadro (Quadrant a)) deriving Show

instance Functor Quadro where
   fmap f (Quadro a b c d) = Quadro (f a) (f b) (f c) (f d)

check :: Point -> Point -> Point -> Bool
check (x,y) (x1,y1) (x2,y2) = (x1 <= x) && (x < x2) && (y1 <= y) && (y < y2)


addNode :: a -> Point -> Quadrant a -> Quadrant a
addNode a p q@(Quadrant p1 p2 Empty) = q {val = Bucket a p} 
addNode a1 p1 q@(Quadrant border1 border2 (Bucket a2 p2)) 
                 | p1 == p2  = q {val = Bucket a1 p1}
                 | otherwise = addNode a1 p1 . addNode a2 p2 $ q {val = QuadTree $ Quadro q1 q2 q3 q4} where
                      (l, u) =  border1
                      (r, b) =  border2
                      (mu, ml) = ((u+b) `div` 2, (l+r) `div` 2)
                      q1 = Quadrant border1 (ml, mu) Empty
                      q2 = Quadrant (ml, u) (r, mu) Empty
                      q3 = Quadrant (l, mu) (ml, b) Empty
                      q4 = Quadrant (ml, mu) border2 Empty
addNode a p q@(Quadrant _ _ (QuadTree tr@(Quadro q1 q2 q3 q4))) 
            | belongs q1 = q {val = QuadTree $ Quadro (f q1) q2 q3 q4}
            | belongs q2 = q {val = QuadTree $ Quadro q1 (f q2) q3 q4}
            | belongs q3 = q {val = QuadTree $ Quadro q1 q2 (f q3) q4}
            | belongs q4 = q {val = QuadTree $ Quadro q1 q2 q3 (f q4)} where
                                                      f = addNode a p
                                                      belongs = check p <$> lu <*> rb
                                                                 
    
