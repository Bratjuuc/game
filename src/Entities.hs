module Entities where
import BaseDataTypes


sack :: Inventory -> Entity
sack inv = Entity {getProperties = [Has inv, Lootable], image = '$' }

player :: Entity
player = Entity {getProperties = [Has [], PlayerControlled, Health 100 100, Powerful 3], image = 'A'}

setImage :: Char -> Entity -> Entity
setImage ch entity = entity {image = ch} 

box :: Entity
box = Entity { getProperties = [Has [], Lootable, Moveable 1, Weak 5, Blocks], image = 'B'}

door :: Entity
door = Entity { getProperties = [Door True True, ]}

