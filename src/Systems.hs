module Systems (cycle) where
import Entities 
import BaseDataTypes
import Util
import Data.Char (toLower)
import Control.Monad (when)

cycle :: [System]
cycle = [playerInteractSystem ,deathSystem]

deathSystem :: System -- Должна запускаться последней
deathSystem world = return world { getEntities = entMap isAlive drop $ getEntities world} where
          drop :: Entity -> Entity
          drop ent = sack $ concatMap has $ getFields defHas ent
          
isAlive :: Entity -> Bool
isAlive ent = (Just False) == (fmap (( > 0) <$> getCurrentHealth) $ getField defHealth ent)

go :: Direction -> System
go

playerInteractSystem :: System
playerInteractSystem world = do
        key <- getChar
        when (_)
        case toLower key of --TODO
           'w' -> go Up' world 
           's' -> go Down' world
           'a' -> go Left' world
           'd' -> go Right' world
           'i' -> displayInventory world--TODO
                 
